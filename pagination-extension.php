<?php

declare(strict_types=1);

return [
    'pagination-extension' => [
        'nav_tag_open' => '<nav><ul class="pagination">',
        'first_tag_open' => '<li class="page-item"><a class="page-link" href="',
        'first_tag_middle' => '">',
        'first_tag_close' => '</a></li>',
        'tag_open' => '<li class="page-item"><a class="page-link" href="',
        'tag_middle' => '">',
        'tag_close' => '</a></li>',
        'current_tag_open' => '<li class="page-item"><a class="page-link-current" href="',
        'current_tag_middle' => '">',
        'current_tag_close' => '<span class="sr-only">(current)</span></a></li>',
        'last_tag_open' => '<li class="page-item"><a class="page-link" href="',
        'last_tag_middle' => '">',
        'last_tag_close' => '</a></li>',
        'nav_tag_close' => '</ul></nav>',

        'links_number' => 2,
    ],
];
