<?php

declare(strict_types=1);

namespace Paneric\Pagination;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class PaginationMiddleware implements MiddlewareInterface
{
    protected ContainerInterface $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $config = $this->container->get('pagination-middleware');

        $uriArray = $this->setUriArray($request);

        $pageCurrent = $uriArray['page_current'];

        $path = $uriArray['path'];

        $rowsNumber = $this->getRowsNumber(
            $request->getAttribute('route_arguments'),
            $config[$request->getAttribute('route_name')]
        );

        $pagination = [
            'pages_number' => (int) ceil($rowsNumber / $config['page_rows_number']),
            'limit' => $config['page_rows_number'],
            'path' => $path,
        ];


        $pageCurrent = min($pageCurrent, $pagination['pages_number']);

        $pagination['page_current'] = $pageCurrent;
        $pagination['offset'] = $pagination['pages_number'] === 0 ? 0 : ($pageCurrent - 1) * $config['page_rows_number'];

        return $handler->handle(
            $request->withAttribute('pagination', $pagination)
        );
    }

    protected function setUriArray(ServerRequestInterface $request): array
    {
        $page = null;

        $routeArguments = $request->getAttribute('route_arguments');

        if (isset($routeArguments) && !empty($routeArguments)) {
            $page = $routeArguments['page'];
        }

        $path = rtrim($request->getUri()->getPath(), '/');

        if ($page === null) {
            return [
                'page_current' => 1,
                'path' => $path . '/'
            ];
        }

        $uriArray = explode('/', $path);

        $pageCurrent = array_pop($uriArray);

        $pageCurrent = $pageCurrent > 0 ? $pageCurrent : 1;

        return [
            'page_current' => $pageCurrent !== 0 ? $pageCurrent : 1,
            'path' => implode('/', $uriArray) . '/',
        ];
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function getRowsNumber(array $routeArguments, $repositoryEntry, array $criteria = null): int
    {
        if (!is_array($repositoryEntry)) {
            $repository = $this->container->get($repositoryEntry);

            return $repository->getRowsNumber($criteria);
        }

        $repository = $this->container->get($repositoryEntry['repository_name']);
        $repositoryEntry['criteria_key'] = $routeArguments[$repositoryEntry['criteria_key']];

        unset($repositoryEntry['repository_name']);

        $criteriaKey = implode('_', $repositoryEntry);

        return count($repository->findBy([
            $criteriaKey => $routeArguments[$repositoryEntry['criteria_value']]
        ]));
    }
}
