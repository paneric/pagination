<?php

declare(strict_types=1);

namespace Paneric\Pagination;

use Paneric\Interfaces\Session\SessionInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class PaginationExtension extends AbstractExtension
{
    public function __construct(protected array $config, protected ?SessionInterface $session = null)
    {
    }

    public function getName(): string
    {
        return 'pagination';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('set_links', [$this, 'setLinks']),
        ];
    }

    public function setLinks(string $path = null, ?array $pagination = null): string
    {
        if ($pagination === null) {
            if ($this->session === null) {
                return '';
            }
            $pagination = $this->session->getData('pagination');
        }

        $pageCurrent = (int) $pagination['page_current'];
        $pagesNumber = (int) $pagination['pages_number'];

        $path = $path ?? $pagination['path'];

        if (isset($this->config['links_number'])) {
            return sprintf(
                '%s%s%s%s%s%s%s%s%s',
                $this->config['nav_tag_open'],
                $this->setFirstPageLink($path, $pageCurrent),
                $this->setPreviousPagesLinks($path, $pageCurrent),
                $this->setBackPageLink( $path, $pageCurrent),
                $this->setCurrentPageTag($path, $pageCurrent),
                $this->setNextPageLink($path, $pageCurrent, $pagesNumber),
                $this->setFollowingPagesLinks($path, $pageCurrent, $pagesNumber),
                $this->setLastPageLink($path, $pageCurrent, $pagesNumber),
                $this->config['nav_tag_close']
            );

        }

        return '';
    }

    protected function setFirstPageLink(string $path, int $pageCurrent): string
    {
        $paginationFirst = '';

        if ($pageCurrent > 1) {
            $paginationFirst = sprintf(
                '%s%s%s%s%s',
                $this->config['first_tag_open'],
                $path . '1',
                $this->config['first_tag_middle'],
                '<<',
                $this->config['first_tag_close']
            );
        }

        return $paginationFirst;
    }

    protected function setPreviousPagesLinks(string $path, int $pageCurrent): string
    {
        $paginationPrev = '';

        for($i = $pageCurrent - $this->config['links_number']; $i <= $pageCurrent; $i++) {
            if ($i > 0 && $i < $pageCurrent) {
                $paginationPrev .= sprintf(
                    '%s%s%s%s%s',
                    $this->config['tag_open'],
                    $path . $i,
                    $this->config['tag_middle'],
                    $i,
                    $this->config['tag_close']
                );
            }
        }

        return $paginationPrev;
    }

    protected function setBackPageLink(string $path, int $pageCurrent): string
    {
        $paginationBack = '';

        if ($pageCurrent > 1) {
            $i = $pageCurrent - 1;

            $paginationBack = sprintf(
                '%s%s%s%s%s',
                $this->config['tag_open'],
                $path . $i,
                $this->config['tag_middle'],
                '<',
                $this->config['tag_close']
            );
        }

        return $paginationBack;
    }

    protected function setCurrentPageTag(string $path, int $pageCurrent): string
    {
        return sprintf(
            '%s%s%s%s%s',
            $this->config['current_tag_open'],
            $path . $pageCurrent,
            $this->config['current_tag_middle'],
            $pageCurrent,
            $this->config['current_tag_close']
        );
    }

    protected function setNextPageLink(string $path, int $pageCurrent, int $pagesNumber): string
    {
        $paginationNext = '';

        if ($pageCurrent < $pagesNumber) {
            $i = $pageCurrent + 1;

            $paginationNext = sprintf(
                '%s%s%s%s%s',
                $this->config['tag_open'],
                $path . $i,
                $this->config['tag_middle'],
                '>',
                $this->config['tag_close']
            );
        }

        return $paginationNext;
    }

    protected function setFollowingPagesLinks(string $path, int $pageCurrent, int $pagesNumber): string
    {
        $paginationFollowing = '';

        for ($i = $pageCurrent + 1; $i <= $pageCurrent + $this->config['links_number']; $i++) {
            if ($i <= $pagesNumber) {
                $paginationFollowing .= sprintf(
                    '%s%s%s%s%s',
                    $this->config['tag_open'],
                    $path . $i,
                    $this->config['tag_middle'],
                    $i,
                    $this->config['tag_close']
                );
            }

        }

        return $paginationFollowing;
    }

    protected function setLastPageLink(string $path, int $pageCurrent, int $pagesNumber): string
    {
        $paginationLast = '';

        if ($pageCurrent < $pagesNumber) {
            $paginationLast = sprintf(
                '%s%s%s%s%s',
                $this->config['last_tag_open'],
                $path . $pagesNumber,
                $this->config['last_tag_middle'],
                '>>',
                $this->config['last_tag_close']
            );
        }

        return $paginationLast;
    }
}
