<?php

declare(strict_types=1);

return [
    'pagination-middleware' => [
        'article.factor_rfqs.show' => FactorRfqRepository::class,

        'article.valid_option_groups.edit' => [
            'repository_entry_name' => OptionGroupVariationRepositoryInterface::class,
            'path' => '/article/valid-option-groups/edit/',
            'page_rows_number' => 100,
        ],

        'page_rows_number' => 20,
    ],
];
